package by.itstep.j2019trainermanagerlashuk.exception;

public class UserExistsException extends Throwable {
    public UserExistsException(String s) {
        super(s);
    }
}
