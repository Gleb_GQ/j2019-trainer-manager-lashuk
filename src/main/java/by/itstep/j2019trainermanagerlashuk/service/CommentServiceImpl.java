package by.itstep.j2019trainermanagerlashuk.service;


import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Comment;
import by.itstep.j2019trainermanagerlashuk.mapper.CommentMapper;
import by.itstep.j2019trainermanagerlashuk.repository.CommentRepository;
import by.itstep.j2019trainermanagerlashuk.repository.TrainerRepository;
import by.itstep.j2019trainermanagerlashuk.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TrainerRepository trainerRepository;
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public List<CommentPreviewDTO> findAll() {
        List<Comment> foundEntities = commentRepository.findAll();
        System.out.println("CommentServiceImpl -> findAll() found: " + foundEntities.size() + " Comments");
        return  commentMapper.mapToPreviewDTOsList(foundEntities);
    }

    @Override
    public CommentFullDTO findById(long id) {
        Comment foundEntity = commentRepository.findById(id);
        System.out.println("CommentServiceImpl -> findById() found: "  + foundEntity);
        return commentMapper.mapToFullDto(foundEntity);
    }

    @Override
    public CommentFullDTO create(CommentCreateDTO createDTO) {
        Comment commentToCreate = commentMapper.mapToEntity(createDTO, userRepository.findById(createDTO.getUserId()),
                trainerRepository.findById(createDTO.getTrainerId()));
        commentToCreate.setPublished(false);
        Comment createdComment = commentRepository.create(commentToCreate);
        System.out.println("CommentServiceImpl -> create() created: "  + createdComment);
        return commentMapper.mapToFullDto(createdComment);
    }

    @Override
    public CommentFullDTO update(CommentUpdateDTO updateDTO) {
        Comment commentToUpdate = commentMapper.mapToEntity(updateDTO);
        Comment existingEntity = commentRepository.findById(updateDTO.getId());
        
        commentToUpdate.setPublished(existingEntity.isPublished());
        commentToUpdate.setTrainer(existingEntity.getTrainer());
        commentToUpdate.setUser(existingEntity.getUser());
        
        Comment updatedComment = commentRepository.update(commentToUpdate);
        System.out.println("CommentServiceImpl -> update() updated: "  + updatedComment);
        return commentMapper.mapToFullDto(updatedComment);
    }

    @Override
    public void deleteById(Long id) {
        commentRepository.deleteById(id);
        System.out.println("CommentServiceImpl -> deleteById() deleted Comment with id: " + id);
    }
}
