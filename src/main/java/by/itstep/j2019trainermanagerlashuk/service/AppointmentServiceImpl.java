package by.itstep.j2019trainermanagerlashuk.service;


import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Appointment;
import by.itstep.j2019trainermanagerlashuk.mapper.AppointmentMapper;
import by.itstep.j2019trainermanagerlashuk.repository.AppointmentRepository;
import by.itstep.j2019trainermanagerlashuk.repository.TrainerRepository;
import by.itstep.j2019trainermanagerlashuk.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentRepository appointmentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TrainerRepository trainerRepository;
    @Autowired
    private AppointmentMapper appointmentMapper;

    @Override
    public List<AppointmentPreviewDTO> findAll() {
        List<Appointment> foundEntities = appointmentRepository.findAll();
        System.out.println("AppointmentServiceImpl -> findAll() found: " + foundEntities.size() + " Appointments");
        return  appointmentMapper.mapToPreviewDTOsList(foundEntities);
    }

    @Override
    public AppointmentFullDTO findById(long id) {
        Appointment foundEntity = appointmentRepository.findById(id);
        System.out.println("AppointmentServiceImpl -> findById() found: "  + foundEntity);
        return appointmentMapper.mapToFullDto(foundEntity);
    }

    @Override
    public AppointmentFullDTO create(AppointmentCreateDTO createDTO) {
        Appointment appointmentToCreate = appointmentMapper.mapToEntity(createDTO, userRepository.findById(createDTO.getUserId()),
                trainerRepository.findById(createDTO.getTrainerId()));

        Appointment createdAppointment = appointmentRepository.create(appointmentToCreate);
        System.out.println("AppointmentServiceImpl -> create() created: "  + createdAppointment);
        return appointmentMapper.mapToFullDto(createdAppointment);
    }

    @Override
    public AppointmentFullDTO update(AppointmentUpdateDTO updateDTO) {
        Appointment appointmentToUpdate = appointmentMapper.mapToEntity(updateDTO, trainerRepository.findById(updateDTO.getTrainerId()));
        Appointment existingEntity = appointmentRepository.findById(updateDTO.getId());

        appointmentToUpdate.setUser(existingEntity.getUser());
        
        Appointment updatedAppointment = appointmentRepository.update(appointmentToUpdate);
        System.out.println("AppointmentServiceImpl -> update() updated: "  + updatedAppointment);
        return appointmentMapper.mapToFullDto(updatedAppointment);
    }

    @Override
    public List<AppointmentPreviewDTO> getLastAppointmentsOfTrainer(Long trainerId, int count) {
        List<Appointment> foundAppointments = appointmentRepository.getLastAppointmentsOfTrainer(trainerId, count);
        return appointmentMapper.mapToPreviewDTOsList(foundAppointments);
    }

    @Override
    public void deleteById(Long id) {
        appointmentRepository.deleteById(id);
        System.out.println("AppointmentServiceImpl -> deleteById() deleted Appointment with id: " + id);
    }
}
