package by.itstep.j2019trainermanagerlashuk.service;

import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentUpdateDTO;

import java.util.List;

public interface CommentService {

    List<CommentPreviewDTO> findAll();

    CommentFullDTO findById(long id);

    CommentFullDTO create(CommentCreateDTO CommentToCreate);

    CommentFullDTO update(CommentUpdateDTO CommentToUpdate);

    void deleteById(Long id);

}
