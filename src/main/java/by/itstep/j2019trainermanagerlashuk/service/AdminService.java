package by.itstep.j2019trainermanagerlashuk.service;

import by.itstep.j2019trainermanagerlashuk.dto.user.UserCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserFullDTO;
import by.itstep.j2019trainermanagerlashuk.exception.UserExistsException;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AdminService extends UserDetailsService {
    UserFullDTO create(UserCreateDTO adminToCreate) throws UserExistsException;
}
