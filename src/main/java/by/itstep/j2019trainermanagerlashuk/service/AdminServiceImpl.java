package by.itstep.j2019trainermanagerlashuk.service;

import by.itstep.j2019trainermanagerlashuk.dto.user.UserCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserFullDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Role;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.exception.UserExistsException;
import by.itstep.j2019trainermanagerlashuk.mapper.UserMapper;
import by.itstep.j2019trainermanagerlashuk.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserMapper userMapper;
    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        User adminFromDb = userRepository.findByEmail(email);

        if(adminFromDb == null || !adminFromDb.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"))){
            throw new UsernameNotFoundException("Admin with email " + email + " doesn't exist.");
        }

        return adminFromDb;
    }

    @Override
    public UserFullDTO create(UserCreateDTO adminToCreateDto) throws UserExistsException {

        User adminFromDb = userRepository.findByEmail(adminToCreateDto.getEmail());

        if(adminFromDb != null){
            throw new UserExistsException("User with email " + adminToCreateDto.getEmail() + " exists.");
        }

        User adminToCreate = userMapper.mapToEntity(adminToCreateDto);
        adminToCreate.setRoles(Collections.singleton(new Role(2L, "ROLE_ADMIN")));
        adminToCreate.setPassword(passwordEncoder.encode(adminToCreateDto.getPassword()));

        User createdAdmin = userRepository.create(adminToCreate);
        System.out.println("AdminServiceImpl -> create() created: "  + createdAdmin);
        return userMapper.mapToFullDto(createdAdmin);
    }
}
