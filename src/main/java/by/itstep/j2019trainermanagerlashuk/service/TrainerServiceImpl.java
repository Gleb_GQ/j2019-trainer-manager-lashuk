package by.itstep.j2019trainermanagerlashuk.service;


import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.mapper.TrainerMapper;
import by.itstep.j2019trainermanagerlashuk.repository.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainerServiceImpl implements TrainerService {

    @Autowired
    private TrainerRepository trainerRepository;
    @Autowired
    private TrainerMapper trainerMapper;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<TrainerPreviewDTO> findAll() {
        List<Trainer> foundEntities = trainerRepository.findAll();
        System.out.println("TrainerServiceImpl -> findAll() found: " + foundEntities.size() + " Trainers");
        return  trainerMapper.mapToPreviewDTOsList(foundEntities);
    }

    @Override
    public TrainerFullDTO findById(long id) {
        Trainer foundEntity = trainerRepository.findById(id);
        System.out.println("TrainerServiceImpl -> findById() found: "  + foundEntity);
        return trainerMapper.mapToFullDto(foundEntity);
    }

    @Override
    public TrainerFullDTO create(TrainerCreateDTO createDTO) {
        Trainer trainerToCreate = trainerMapper.mapToEntity(createDTO);
        trainerToCreate.setPassword(passwordEncoder.encode(createDTO.getPassword()));
        Trainer createdTrainer = trainerRepository.create(trainerToCreate);
        System.out.println("TrainerServiceImpl -> create() created: "  + createdTrainer);
        return trainerMapper.mapToFullDto(createdTrainer);
    }

    @Override
    public TrainerFullDTO update(TrainerUpdateDTO updateDTO) {
        Trainer trainerToUpdate = trainerMapper.mapToEntity(updateDTO);
        Trainer existingEntity = trainerRepository.findById(updateDTO.getId());

        trainerToUpdate.setExperience(existingEntity.getExperience());
        trainerToUpdate.setComments(existingEntity.getComments());
        trainerToUpdate.setAppointments(existingEntity.getAppointments());
        
        Trainer updatedTrainer = trainerRepository.update(trainerToUpdate);
        System.out.println("TrainerServiceImpl -> update() updated: "  + updatedTrainer);
        return trainerMapper.mapToFullDto(updatedTrainer);
    }

    @Override
    public void deleteById(Long id) {
        trainerRepository.deleteById(id);
        System.out.println("TrainerServiceImpl -> deleteById() deleted Trainer with id: " + id);
    }

    @Override
    public TrainerFullDTO findByEmail(String email) {

        Trainer trainerFromdb = trainerRepository.findByEmail(email);
        return trainerMapper.mapToFullDto(trainerFromdb);
    }

    @Override
    public List<TrainerPreviewDTO> findTopTrainers(int count) {
        List<Trainer> trainersFromDb = trainerRepository.findTopTrainers(count);
        System.out.println("TrainerServiceImpl -> findTopTrainers() found: " + trainersFromDb.size() + " Trainers");
        return  trainerMapper.mapToPreviewDTOsList(trainersFromDb);
    }

    @Override
    public UserDetails loadUserByUsername(String trainerEmail) throws UsernameNotFoundException {
        Trainer trainerFromDb = trainerRepository.findByEmail(trainerEmail);

        if(trainerFromDb == null){
            throw new UsernameNotFoundException("Trainer with email: " + trainerEmail + " doesn't exist.");
        }

        return trainerFromDb;
    }
}
