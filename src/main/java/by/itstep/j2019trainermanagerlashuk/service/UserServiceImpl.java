package by.itstep.j2019trainermanagerlashuk.service;

import by.itstep.j2019trainermanagerlashuk.dto.user.UserCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.entity.Role;
import by.itstep.j2019trainermanagerlashuk.exception.UserExistsException;
import by.itstep.j2019trainermanagerlashuk.mapper.UserMapper;
import by.itstep.j2019trainermanagerlashuk.mapper.UserMapper;
import by.itstep.j2019trainermanagerlashuk.repository.UserRepository;
import by.itstep.j2019trainermanagerlashuk.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<UserPreviewDTO> findAll() {
        List<User> foundEntities = userRepository.findAll();
        System.out.println("UserServiceImpl -> findAll() found: " + foundEntities.size() + " users");
        return  userMapper.mapToPreviewDTOsList(foundEntities);
    }

    @Override
    public UserFullDTO findById(long id) {
        User foundEntity = userRepository.findById(id);
        System.out.println("UserServiceImpl -> findById() found: "  + foundEntity);
        return userMapper.mapToFullDto(foundEntity);
    }

    @Override
    public UserFullDTO create(UserCreateDTO createDTO) throws UserExistsException {

        User userFromDb = userRepository.findByEmail(createDTO.getEmail());

        if(userFromDb != null){
            throw new UserExistsException("User with email " + createDTO.getEmail() + " exists.");
        }

        User userToCreate = userMapper.mapToEntity(createDTO);
        userToCreate.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        userToCreate.setPassword(passwordEncoder.encode(createDTO.getPassword()));

        User createdUser = userRepository.create(userToCreate);
        System.out.println("UserServiceImpl -> create() created: "  + createdUser);
        return userMapper.mapToFullDto(createdUser);
    }

    @Override
    public UserFullDTO update(UserUpdateDTO updateDTO) {
        User userToUpdate = userMapper.mapToEntity(updateDTO);
        User existingEntity = userRepository.findById(updateDTO.getId());

        userToUpdate.setRoles(existingEntity.getRoles());
        userToUpdate.setComments(existingEntity.getComments());
        userToUpdate.setAppointments(existingEntity.getAppointments());

        User updatedUser = userRepository.update(userToUpdate);
        System.out.println("UserServiceImpl -> update() updated: "  + updatedUser);
        return userMapper.mapToFullDto(updatedUser);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
        System.out.println("UserServiceImpl -> deleteById() deleted user with id: " + id);
    }

    @Override
    public UserFullDTO findByEmail(String email) {
        User userFromDb = userRepository.findByEmail(email);
        return userMapper.mapToFullDto(userFromDb);
    }

    @Override
    public UserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException {

        User userFromDb = userRepository.findByEmail(userEmail);

        if(userFromDb == null){
            throw new UsernameNotFoundException("User with email " + userEmail + " doesn't exist.");
        }

        return userFromDb;
    }
}
