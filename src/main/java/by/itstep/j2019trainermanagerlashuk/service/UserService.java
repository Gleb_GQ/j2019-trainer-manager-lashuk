package by.itstep.j2019trainermanagerlashuk.service;

import by.itstep.j2019trainermanagerlashuk.dto.user.UserCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.exception.UserExistsException;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService{

    UserFullDTO findByEmail(String email);

    List<UserPreviewDTO> findAll();

    UserFullDTO findById(long id);

    UserFullDTO create(UserCreateDTO UserToCreate) throws UserExistsException;

    UserFullDTO update(UserUpdateDTO UserToUpdate);

    void deleteById(Long id);

}
