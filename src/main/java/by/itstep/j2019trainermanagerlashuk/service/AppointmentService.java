package by.itstep.j2019trainermanagerlashuk.service;

import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentUpdateDTO;

import java.util.List;

public interface AppointmentService {

    List<AppointmentPreviewDTO> findAll();

    AppointmentFullDTO findById(long id);

    AppointmentFullDTO create(AppointmentCreateDTO AppointmentToCreate);

    AppointmentFullDTO update(AppointmentUpdateDTO AppointmentToUpdate);

    List<AppointmentPreviewDTO> getLastAppointmentsOfTrainer(Long trainerId, int count);

    void deleteById(Long id);
    
}
