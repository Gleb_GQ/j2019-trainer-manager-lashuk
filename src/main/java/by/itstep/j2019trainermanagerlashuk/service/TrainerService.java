package by.itstep.j2019trainermanagerlashuk.service;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface TrainerService extends UserDetailsService {

    TrainerFullDTO findByEmail(String email);

    List<TrainerPreviewDTO> findAll();

    TrainerFullDTO findById(long id);

    TrainerFullDTO create(TrainerCreateDTO createDTO);

    TrainerFullDTO update(TrainerUpdateDTO updateDTO);

    void deleteById(Long id);

    List<TrainerPreviewDTO> findTopTrainers(int count);

}
