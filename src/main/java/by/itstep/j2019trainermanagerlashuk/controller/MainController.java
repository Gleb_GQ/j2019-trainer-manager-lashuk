package by.itstep.j2019trainermanagerlashuk.controller;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.service.TrainerService;
import by.itstep.j2019trainermanagerlashuk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.interfaces.DSAPrivateKey;
import java.util.List;


@Controller
public class MainController {

    @Autowired
    TrainerService trainerService;

    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model){
        List<TrainerPreviewDTO> topTrainers = trainerService.findTopTrainers(4);
        model.addAttribute("trainers", topTrainers);
        return "index";
    }


}
