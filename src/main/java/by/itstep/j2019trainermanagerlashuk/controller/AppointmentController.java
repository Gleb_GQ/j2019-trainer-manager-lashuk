package by.itstep.j2019trainermanagerlashuk.controller;

import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerFullDTO;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.service.AppointmentService;
import by.itstep.j2019trainermanagerlashuk.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/trainer")
public class AppointmentController{

    @Autowired
    TrainerService trainerService;

    @Autowired
    AppointmentService appointmentService;

    @RequestMapping("/appoint/{username}")
    public String getMakeAppointmentForm(@PathVariable("username") String username, Model model){

        AppointmentCreateDTO appointmentCreateDTO = new AppointmentCreateDTO();

        model.addAttribute("appointment", appointmentCreateDTO);
        model.addAttribute("trainerUsername", username);

        return "make-appointment";

    }

    @PostMapping("/appoint/{username}")
    public String saveAppointment(@PathVariable("username") String username, @ModelAttribute("appointment") AppointmentCreateDTO appointment, Model model){
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        TrainerFullDTO trainer = trainerService.findByEmail(username);

        appointment.setTrainerId(trainer.getId());
        appointment.setUserId(currentUser.getId());

        appointmentService.create(appointment);

        return "index";
    }

}
