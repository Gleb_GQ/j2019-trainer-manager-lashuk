package by.itstep.j2019trainermanagerlashuk.controller;

import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerFullDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Appointment;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.repository.AppointmentRepository;
import by.itstep.j2019trainermanagerlashuk.service.AppointmentService;
import by.itstep.j2019trainermanagerlashuk.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/trainer")
public class TrainerController {

    @Autowired
    TrainerService trainerService;
    @Autowired
    AppointmentService appointmentService;

    @GetMapping("/profile/{username}")
    public String getTrainerPage(@PathVariable("username") String username, Model model){

        boolean isCanEdit = checkIfCurrentUserCanEditTrainerProfile(username);

        TrainerFullDTO trainer = trainerService.findByEmail(username);

        List<AppointmentPreviewDTO> appointments = trainer.getAppointments();
        trainer.setAppointments(appointments.subList(0, Math.min(appointments.size(), 2)));

        List<CommentPreviewDTO> comments = trainer.getComments();
        trainer.setComments(comments.subList(0, Math.min(comments.size(), 3)));

        model.addAttribute("trainer", trainer);
        model.addAttribute("canEdit", isCanEdit);
        return "trainer-profile";
    }

    private boolean checkIfCurrentUserCanEditTrainerProfile(String requestedUserName){
        if(isAdmin()){
            return true;
        }else{
            var auth = SecurityContextHolder.getContext().getAuthentication();
            var currentUser = auth.getPrincipal();
            var currentUserName = currentUser; // if get proncipal returns string
            if(currentUser instanceof UserDetails){
                currentUserName = ((UserDetails)currentUser).getUsername();
            }
            return requestedUserName.equals(currentUserName);
        }
    }

    private boolean isAdmin(){
        return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest().isUserInRole("ADMIN");
    }

}
