package by.itstep.j2019trainermanagerlashuk.controller;

import by.itstep.j2019trainermanagerlashuk.dto.user.UserFullDTO;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/users")
public class ProfileController {

    @Autowired
    UserService userService;

    @GetMapping("{username}")
    @PreAuthorize("((authentication.principal.username == #username) || hasRole('ADMIN'))")
    public String usersGet(@PathVariable("username") String username, Model model){

        UserFullDTO user = userService.findByEmail(username);
        model.addAttribute("user", user);


        return "profile";
    }

    @GetMapping("/update/{username}")
    @PreAuthorize("(authentication.principal.username == #username) || hasRole('ADMIN')")
    public String getUpdatePage(@PathVariable("username") String username, Model model){

        User user = (User)userService.loadUserByUsername(username);
        model.addAttribute("username", user.getName());

        return "update";
    }

}
