package by.itstep.j2019trainermanagerlashuk.controller;

import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserCreateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.service.CommentService;
import by.itstep.j2019trainermanagerlashuk.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/trainer")
public class CommentController {
    @Autowired
    CommentService commentService;
    @Autowired
    TrainerService trainerService;

    @GetMapping("/comment/{username}")
    public String getLeaveCommentForm(@PathVariable("username") String username, Model model){

        CommentCreateDTO commentCreateDTO = new CommentCreateDTO();

        model.addAttribute("comment", commentCreateDTO);
        model.addAttribute("trainerUsername", username);

        return "leave-comment";
    }

    @PostMapping("/comment/{username}")
    public String saveComment(@PathVariable("username") String username, @ModelAttribute("comment") CommentCreateDTO comment, Model model){
        User currentUser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        TrainerFullDTO trainer = trainerService.findByEmail(username);

        comment.setTrainerId(trainer.getId());
        comment.setUserId(currentUser.getId());

        commentService.create(comment);

        return "index";
    }

}
