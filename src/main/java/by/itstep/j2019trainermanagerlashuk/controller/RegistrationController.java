package by.itstep.j2019trainermanagerlashuk.controller;

import by.itstep.j2019trainermanagerlashuk.dto.user.UserCreateDTO;
import by.itstep.j2019trainermanagerlashuk.exception.UserExistsException;
import by.itstep.j2019trainermanagerlashuk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegistrationController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String registration(Model model) {
        model.addAttribute("user", new UserCreateDTO());

        return "registration";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/registration")
    public String registrateUser(@ModelAttribute("user") UserCreateDTO user, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "registration";
        }

        //TODO make pass conf

        try{
            userService.create(user);
        }catch (UserExistsException e) {
            model.addAttribute("emailError", "User with this email already exists");
            return "registration";
        }

        return "redirect:/";
    }
}
