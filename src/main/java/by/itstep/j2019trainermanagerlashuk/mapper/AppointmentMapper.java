package by.itstep.j2019trainermanagerlashuk.mapper;

import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Appointment;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class AppointmentMapper {

    @Autowired
    private TrainerMapper trainerMapper;
    @Autowired
    private  UserMapper userMapper;

    public List<AppointmentPreviewDTO> mapToPreviewDTOsList(List<Appointment> appointments) {
        List<AppointmentPreviewDTO> dtos = new ArrayList<>();
        for(Appointment entity : appointments){
            AppointmentPreviewDTO dto = new AppointmentPreviewDTO();
            dto.setId(entity.getId());
            dto.setComment(entity.getComment());
            dto.setAppointmentDate(entity.getAppointmentDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
            dto.setTrainer(trainerMapper.mapToPreviewDTO(entity.getTrainer()));
            dto.setUser(userMapper.mapToPreviewDTO(entity.getUser()));
            dtos.add(dto);
        }
        return  dtos;
    }

    public AppointmentFullDTO mapToFullDto(Appointment entity) {
        AppointmentFullDTO fullDTO = new AppointmentFullDTO();
        fullDTO.setId(entity.getId());
        fullDTO.setComment(entity.getComment());
        fullDTO.setAppointmentDate(entity.getAppointmentDate());
        fullDTO.setTrainer(trainerMapper.mapToPreviewDTO(entity.getTrainer()));
        fullDTO.setUser(userMapper.mapToPreviewDTO(entity.getUser()));
        return fullDTO;
    }

    public Appointment mapToEntity(AppointmentCreateDTO createDTO, User user, Trainer trainer) {
        return Appointment.builder()
                .comment(createDTO.getComment())
                .appointmentDate(createDTO.getAppointmentDate())
                .user(user)
                .trainer(trainer)
                .build();
    }

    public Appointment mapToEntity(AppointmentUpdateDTO updateDTO, Trainer trainer) {
        return Appointment.builder()
                .id(updateDTO.getId())
                .comment(updateDTO.getComment())
                .appointmentDate(updateDTO.getAppointmentDate())
                .trainer(trainer)
                .build();
    }
}

