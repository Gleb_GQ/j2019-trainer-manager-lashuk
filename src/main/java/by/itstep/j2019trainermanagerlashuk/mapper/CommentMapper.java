package by.itstep.j2019trainermanagerlashuk.mapper;

import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Comment;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentMapper {

    @Autowired
    private TrainerMapper trainerMapper;
    @Autowired
    private  UserMapper userMapper;

    public List<CommentPreviewDTO> mapToPreviewDTOsList(List<Comment> comments) {
        List<CommentPreviewDTO> dtos = new ArrayList<>();
        for(Comment entity : comments){
            CommentPreviewDTO dto = new CommentPreviewDTO();
            dto.setId(entity.getId());
            dto.setRatingOfTrainer(entity.getRatingOfTrainer());
            dto.setMessage(entity.getMessage());
            dto.setTrainer(trainerMapper.mapToPreviewDTO(entity.getTrainer()));
            dto.setUser(userMapper.mapToPreviewDTO(entity.getUser()));
            dtos.add(dto);
        }
        return dtos;
    }

    public CommentFullDTO mapToFullDto(Comment foundEntity) {
        CommentFullDTO fullDTO = new CommentFullDTO();
        fullDTO.setId(foundEntity.getId());
        fullDTO.setPublished(foundEntity.isPublished());
        fullDTO.setRatingOfTrainer(foundEntity.getRatingOfTrainer());
        fullDTO.setMessage(foundEntity.getMessage());
        fullDTO.setTrainer(trainerMapper.mapToPreviewDTO(foundEntity.getTrainer()));
        fullDTO.setUser(userMapper.mapToPreviewDTO(foundEntity.getUser()));
        return fullDTO;
    }

    public Comment mapToEntity(CommentCreateDTO createDTO, User user, Trainer trainer) {
        return Comment.builder()
                .message(createDTO.getMessage())
                .ratingOfTrainer(createDTO.getRatingOfTrainer())
                .user(user)
                .trainer(trainer)
                .build();
    }

    public Comment mapToEntity(CommentUpdateDTO updateDTO) {
        return Comment.builder()
                .id(updateDTO.getId())
                .message(updateDTO.getMessage())
                .ratingOfTrainer(updateDTO.getRatingOfTrainer())
                .build();
    }
}
