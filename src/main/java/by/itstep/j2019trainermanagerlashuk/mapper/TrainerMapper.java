package by.itstep.j2019trainermanagerlashuk.mapper;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TrainerMapper {
    @Autowired
    private AppointmentMapper appointmentMapper;
    @Autowired
    private CommentMapper commentMapper;

    public TrainerPreviewDTO mapToPreviewDTO(Trainer entity){
        TrainerPreviewDTO dto = new TrainerPreviewDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setImageUrl(entity.getImageUrl());
        dto.setDescription(entity.getDescription());
        dto.setEmail(entity.getEmail());
        dto.setExperience(entity.getExperience());
        return dto;
    }

    public List<TrainerPreviewDTO> mapToPreviewDTOsList(List<Trainer> foundEntities) {
        List<TrainerPreviewDTO> dtos = new ArrayList<>();
        for(Trainer entity : foundEntities){
            dtos.add(mapToPreviewDTO(entity));
        }
        return dtos;
    }

    public TrainerFullDTO mapToFullDto(Trainer entity) {
        TrainerFullDTO fullDTO = new TrainerFullDTO();
        fullDTO.setId(entity.getId());
        fullDTO.setName(entity.getName());
        fullDTO.setImageUrl(entity.getImageUrl());
        fullDTO.setDescription(entity.getDescription());
        fullDTO.setEmail(entity.getEmail());
        fullDTO.setExperience(entity.getExperience());
        fullDTO.setAppointments(appointmentMapper.mapToPreviewDTOsList(entity.getAppointments()));
        fullDTO.setComments(commentMapper.mapToPreviewDTOsList(entity.getComments()));
        return fullDTO;
    }

    public Trainer mapToEntity(TrainerCreateDTO createDTO) {
        return Trainer.builder()
                .name(createDTO.getName())
                .email(createDTO.getEmail())
                .imageUrl(createDTO.getImageUrl())
                .experience(createDTO.getExperience())
                .description(createDTO.getDescription())
                .password(createDTO.getPassword())
                .build();
    }

    public Trainer mapToEntity(TrainerUpdateDTO updateDTO) {
        return Trainer.builder()
                .name(updateDTO.getName())
                .email(updateDTO.getEmail())
                .imageUrl(updateDTO.getImageUrl())
                .id(updateDTO.getId())
                .description(updateDTO.getDescription())
                .password(updateDTO.getPassword())
                .build();
    }
}
