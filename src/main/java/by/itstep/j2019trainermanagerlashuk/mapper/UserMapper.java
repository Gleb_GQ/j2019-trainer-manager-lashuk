package by.itstep.j2019trainermanagerlashuk.mapper;

import by.itstep.j2019trainermanagerlashuk.dto.user.UserCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserFullDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserUpdateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Appointment;
import by.itstep.j2019trainermanagerlashuk.entity.Comment;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    @Autowired
    private AppointmentMapper appointmentMapper;
    @Autowired
    private CommentMapper commentMapper;

    public UserPreviewDTO mapToPreviewDTO(User entity){
        UserPreviewDTO previewDTO = new UserPreviewDTO();
        previewDTO.setId(entity.getId());
        previewDTO.setName(entity.getName());
        previewDTO.setLastName(entity.getLastName());
        previewDTO.setEmail(entity.getEmail());
        previewDTO.setImageUrl(entity.getImageUrl());
        return previewDTO;
    }

    public List<UserPreviewDTO> mapToPreviewDTOsList(List<User> foundEntities) {
        List<UserPreviewDTO> dtos = new ArrayList<>();
        for(User entity: foundEntities){
            dtos.add(mapToPreviewDTO(entity));
        }
        return dtos;
    }

    public UserFullDTO mapToFullDto(User entity) {
        UserFullDTO userFullDTO = new UserFullDTO();
        userFullDTO.setId(entity.getId());
        userFullDTO.setAppointments(appointmentMapper.mapToPreviewDTOsList(entity.getAppointments()));
        userFullDTO.setComments(commentMapper.mapToPreviewDTOsList(entity.getComments()));
        userFullDTO.setEmail(entity.getEmail());
        userFullDTO.setImageUrl(entity.getImageUrl());
        userFullDTO.setLastName(entity.getLastName());
        userFullDTO.setName(entity.getName());
        userFullDTO.setRoles(entity.getRoles());
        return userFullDTO;
    }

    public User mapToEntity(UserCreateDTO createDTO) {
        User user = User.builder()
                .name(createDTO.getName())
                .lastName(createDTO.getLastName())
                .email(createDTO.getEmail())
                .imageUrl(createDTO.getImageUrl())
                .password(createDTO.getPassword())
                .build();
        return user;
    }

    public User mapToEntity(UserUpdateDTO updateDTO) {
        User user = User.builder()
                .id(updateDTO.getId())
                .name(updateDTO.getName())
                .lastName(updateDTO.getLastName())
                .email(updateDTO.getEmail())
                .imageUrl(updateDTO.getImageUrl())
                .password(updateDTO.getPassword())
                .build();
        return user;
    }
}
