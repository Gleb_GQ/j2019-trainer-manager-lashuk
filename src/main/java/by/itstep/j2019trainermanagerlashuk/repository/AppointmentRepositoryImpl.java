package by.itstep.j2019trainermanagerlashuk.repository;

import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Appointment;
import by.itstep.j2019trainermanagerlashuk.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class AppointmentRepositoryImpl implements AppointmentRepository {
    @Override
    public List<Appointment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Appointment> foundList = em.createNativeQuery("SELECT * FROM `appointment`", Appointment.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" Appointments");
        return foundList;
    }

    @Override
    public List<Appointment> getLastAppointmentsOfTrainer(Long trainerId, int count) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Appointment> foundList = em.createNativeQuery(String.format("SELECT * FROM `appointment` ORDER BY `appointment_date` DESC " +
                "WHERE trainer_id = %d LIMIT %d", trainerId, count), Appointment.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" Appointments");
        return foundList;
    }

    @Override
    public Appointment findById(long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        Appointment foundAppointment = em.find(Appointment.class, id);
        Hibernate.initialize(foundAppointment.getTrainer());
        Hibernate.initialize(foundAppointment.getUser());
        em.close();
        System.out.println("Found Appointment: " + foundAppointment);
        return  foundAppointment;
    }

    @Override
    public Appointment create(Appointment appointmentToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(appointmentToCreate);

        em.getTransaction().commit();
        em.close();
        System.out.println("Appointment was created. Id: " + appointmentToCreate.getId());
        return appointmentToCreate;
    }

    @Override
    public Appointment update(Appointment appointmentToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Поменял persist на merge,
        // т.к была ошибка при попытке вызвать persist с detached объектом
        em.merge(appointmentToUpdate);

        em.getTransaction().commit();
        em.close();
        System.out.println("Appointment was created. Id: " + appointmentToUpdate.getId());
        return appointmentToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        Appointment appointmentToDelete = em.find(Appointment.class, id);
        em.remove(appointmentToDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM `appointment`").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
