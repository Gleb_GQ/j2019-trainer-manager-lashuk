package by.itstep.j2019trainermanagerlashuk.repository;

import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import org.springframework.stereotype.Repository;

import java.util.List;



public interface TrainerRepository {

    Trainer findByEmail(String email);

    List<Trainer> findAll();

    Trainer findById(long id);

    Trainer create(Trainer trainerToCreate);

    Trainer update(Trainer trainerToUpdate);

    void deleteById(Long id);

    void deleteAll();

    List<Trainer> findTopTrainers(int count);
}
