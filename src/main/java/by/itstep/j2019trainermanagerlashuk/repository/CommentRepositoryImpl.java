package by.itstep.j2019trainermanagerlashuk.repository;

import by.itstep.j2019trainermanagerlashuk.entity.Comment;
import by.itstep.j2019trainermanagerlashuk.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CommentRepositoryImpl implements CommentRepository {
    @Override
    public List<Comment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Comment> foundList = em.createNativeQuery("SELECT * FROM `Comment`", Comment.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" Comments");
        return foundList;
    }

    @Override
    public Comment findById(long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        Comment foundComment = em.find(Comment.class, id);
        Hibernate.initialize(foundComment.getTrainer());
        Hibernate.initialize(foundComment.getUser());
        em.close();
        System.out.println("Found Comment: " + foundComment);
        return  foundComment;
    }

    @Override
    public Comment create(Comment commentToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(commentToCreate);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was created. Id: " + commentToCreate.getId());
        return commentToCreate;
    }

    @Override
    public Comment update(Comment commentToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Поменял persist на merge,
        // т.к была ошибка при попытке вызвать persist с detached объектом
        em.merge(commentToUpdate);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was created. Id: " + commentToUpdate.getId());
        return commentToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        Comment commentToDelete = em.find(Comment.class, id);
        em.remove(commentToDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM `comment`").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
