package by.itstep.j2019trainermanagerlashuk.repository;

import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.Embeddable;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Override
    public User findByEmail(String email){
        EntityManager em = EntityManagerUtils.getEntityManager();

        User foundUser;
        try{
            foundUser =  (User)em.createNativeQuery(String.format("SELECT * FROM `user` WHERE email = \"%s\"", email), User.class).getSingleResult();

            Hibernate.initialize(foundUser.getComments());
            Hibernate.initialize(foundUser.getAppointments());
        }catch (NoResultException ex){
            foundUser = null;
        }


        em.close();
        return foundUser;
    }

    @Override
    public List<User> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

         List<User> foundList = em.createNativeQuery("SELECT * FROM `user`", User.class)
                    .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" Users");
        return foundList;
    }

    @Override
    public User findById(long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        User foundUser = em.find(User.class, id);

        Hibernate.initialize(foundUser.getAppointments());
        Hibernate.initialize(foundUser.getComments());
        em.close();
        System.out.println("Found User: " + foundUser);
        return  foundUser;
    }

    @Override
    public User create(User userToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(userToCreate);

        em.getTransaction().commit();
        em.close();
        System.out.println("User was created. Id: " + userToCreate.getId());
        return userToCreate;
    }

    @Override
    public User update(User UserToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Поменял persist на merge,
        // т.к была ошибка при попытке вызвать persist с detached объектом
        em.merge(UserToUpdate);

        em.getTransaction().commit();
        em.close();
        System.out.println("User was updated. Id: " + UserToUpdate.getId());
        return UserToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        User userToDelete = em.find(User.class, id);
        em.remove(userToDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM `User`").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
