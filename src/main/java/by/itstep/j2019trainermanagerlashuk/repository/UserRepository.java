package by.itstep.j2019trainermanagerlashuk.repository;

import by.itstep.j2019trainermanagerlashuk.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository {

    List<User> findAll();

    User findById(long id);

    User findByEmail(String email);

    User create(User userToCreate);

    User update(User userToUpdate);

    void deleteById(Long id);

    void deleteAll();
}
