package by.itstep.j2019trainermanagerlashuk.repository;

import by.itstep.j2019trainermanagerlashuk.entity.Comment;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CommentRepository {

    List<Comment> findAll();

    Comment findById(long id);

    Comment create(Comment commentToCreate);

    Comment update(Comment commentToUpdate);

    void deleteById(Long id);

    void deleteAll();
}
