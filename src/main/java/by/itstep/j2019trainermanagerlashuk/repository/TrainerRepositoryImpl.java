package by.itstep.j2019trainermanagerlashuk.repository;

import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class TrainerRepositoryImpl implements TrainerRepository {

    @Override
    public Trainer findByEmail(String email){
        EntityManager em = EntityManagerUtils.getEntityManager();

        Trainer foundTrainer;
        try{
            foundTrainer =  (Trainer) em.createNativeQuery(String.format("SELECT * FROM `trainer` WHERE email = \"%s\"", email), Trainer.class).getSingleResult();

            Hibernate.initialize(foundTrainer.getComments());
            Hibernate.initialize(foundTrainer.getAppointments());
        }catch (NoResultException ex){
            foundTrainer = null;
        }


        em.close();
        return foundTrainer;
    }

    @Override
    public List<Trainer> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Trainer> foundList = em.createNativeQuery("SELECT * FROM `trainer`", Trainer.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" Trainers");
        return foundList;
    }

    @Override
    public List<Trainer> findTopTrainers(int count) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Trainer> foundList = em.createNativeQuery(String.format("SELECT * FROM `trainer` ORDER BY `experience` DESC LIMIT %d", count), Trainer.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " trainers");
        return foundList;
    }

    @Override
    public Trainer findById(long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        Trainer foundTrainer = em.find(Trainer.class, id);
        Hibernate.initialize(foundTrainer.getAppointments());
        Hibernate.initialize(foundTrainer.getComments());
        em.close();
        System.out.println("Found Trainer: " + foundTrainer);
        return  foundTrainer;
    }


    @Override
    public Trainer create(Trainer trainerToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(trainerToCreate);

        em.getTransaction().commit();
        em.close();
        System.out.println("Trainer was created. Id: " + trainerToCreate.getId());
        return trainerToCreate;
    }

    @Override
    public Trainer update(Trainer trainerToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Поменял persist на merge,
        // т.к была ошибка при попытке вызвать persist с detached объектом
        em.merge(trainerToUpdate);

        em.getTransaction().commit();
        em.close();
        System.out.println("Trainer was created. Id: " + trainerToUpdate.getId());
        return trainerToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        Trainer trainerToDelete = em.find(Trainer.class, id);
        em.remove(trainerToDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM `trainer`").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
