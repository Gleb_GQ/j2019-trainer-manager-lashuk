package by.itstep.j2019trainermanagerlashuk.repository;

import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Appointment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentRepository {

    List<Appointment> findAll();

    List<Appointment> getLastAppointmentsOfTrainer(Long trainerId, int count);

    Appointment findById(long id);

    Appointment create(Appointment appointmentToCreate);

    Appointment update(Appointment appointmentToUpdate);

    void deleteById(Long id);

    void deleteAll();
}
