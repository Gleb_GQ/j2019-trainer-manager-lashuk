package by.itstep.j2019trainermanagerlashuk.config;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerCreateDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserCreateDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.exception.UserExistsException;
import by.itstep.j2019trainermanagerlashuk.repository.TrainerRepositoryImpl;
import by.itstep.j2019trainermanagerlashuk.repository.UserRepository;
import by.itstep.j2019trainermanagerlashuk.service.AdminService;
import by.itstep.j2019trainermanagerlashuk.service.TrainerService;
import org.hibernate.dialect.function.DB2SubstringFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader implements ApplicationRunner {

    @Lazy
    @Autowired
    private AdminService adminService;

    @Lazy
    @Autowired
    private TrainerService trainerService;

    public void run(ApplicationArguments args) {
        createFirstAdmin();
        createFirstTrainers();
    }

    private void createFirstAdmin(){
        UserCreateDTO adminDto = new UserCreateDTO();

        adminDto.setName("Gleb");
        adminDto.setLastName("Lashuk");
        adminDto.setEmail("gleb.gq@gmail.com");
        adminDto.setImageUrl("https://images.unsplash.com/photo-1542909192-2f2241a99c9d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60");
        adminDto.setPassword("Gleb20021009");

        try {
            adminService.create(adminDto);
        } catch (UserExistsException e) {
            e.printStackTrace();
        }
    }

    private void createFirstTrainers(){
        List<String> trainersAvatarUrls = populateListOfAvatars();

        for(int i = 1; i <= 10; ++i){
            TrainerCreateDTO createDTO = createRandomTrainer("test" + i +"@gmail.com");
            createDTO.setName("Test Trainer Name #" + i);
            createDTO.setImageUrl(trainersAvatarUrls.get(i - 1));
            trainerService.create(createDTO);
        }
    }

    private List<String> populateListOfAvatars(){
        List<String> urls = new ArrayList<>();
        urls.add("https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80");
        urls.add("https://images.unsplash.com/photo-1521119989659-a83eee488004?ixlib=rb-1.2.1&auto=format&fit=crop&w=664&q=80");
        urls.add("https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80");
        urls.add("https://images.unsplash.com/photo-1552374196-c4e7ffc6e126?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60");
        urls.add("https://images.unsplash.com/photo-1528892952291-009c663ce843?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60");
        urls.add("https://images.unsplash.com/photo-1529626455594-4ff0802cfb7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80");
        urls.add("https://images.unsplash.com/photo-1551843073-4a9a5b6fcd5f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60");
        urls.add("https://images.unsplash.com/photo-1554151228-14d9def656e4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=333&q=80");
        urls.add("https://images.unsplash.com/photo-1493106819501-66d381c466f1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60");
        urls.add("https://images.unsplash.com/photo-1519345182560-3f2917c472ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60");
        return urls;
    }

    private TrainerCreateDTO createRandomTrainer(String email){
        TrainerCreateDTO trainerCreateDTO = new TrainerCreateDTO();

        trainerCreateDTO.setDescription("BODYBUILDER 100 LVL");
        trainerCreateDTO.setExperience((int)(Math.random() * 10));
        trainerCreateDTO.setEmail(email);
        trainerCreateDTO.setPassword("test");

        return trainerCreateDTO;
    }
}