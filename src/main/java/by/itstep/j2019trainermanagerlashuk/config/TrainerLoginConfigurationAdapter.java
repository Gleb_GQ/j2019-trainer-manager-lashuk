package by.itstep.j2019trainermanagerlashuk.config;

import by.itstep.j2019trainermanagerlashuk.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@Order(1)
public class TrainerLoginConfigurationAdapter extends WebSecurityConfigurerAdapter {

    @Autowired
    TrainerService trainerService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .antMatcher("/trainer/login/**")
                .authorizeRequests()
                .antMatchers("/trainer/login/**").authenticated()
                .antMatchers("/login").not().fullyAuthenticated()
                .antMatchers("/trainer/profile/**").permitAll()
                .antMatchers("/trainer/**").hasRole("TRAINER")
                .antMatchers("/trainer/**").hasRole("ADMIN")
                .and()
                    .formLogin()
                    .loginPage("/trainer/login")
                    .defaultSuccessUrl("/index")
                    .permitAll()
                .and()
                    .logout()
                    .logoutUrl("/logout")
                    .deleteCookies("JSESSIONID")
                    .permitAll()
                    .logoutSuccessUrl("/trainer/login")
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(trainerService).passwordEncoder(bCryptPasswordEncoder());
    }
}
