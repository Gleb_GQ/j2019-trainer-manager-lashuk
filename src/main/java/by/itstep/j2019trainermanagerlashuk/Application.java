package by.itstep.j2019trainermanagerlashuk;

import by.itstep.j2019trainermanagerlashuk.service.UserService;
import by.itstep.j2019trainermanagerlashuk.util.EntityManagerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
