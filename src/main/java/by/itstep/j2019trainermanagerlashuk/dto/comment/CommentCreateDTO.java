package by.itstep.j2019trainermanagerlashuk.dto.comment;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserPreviewDTO;
import lombok.Data;

@Data
public class CommentCreateDTO {

    private int ratingOfTrainer;
    private Long userId;
    private Long trainerId;
    private String message;

}
