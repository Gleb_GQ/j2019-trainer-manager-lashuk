package by.itstep.j2019trainermanagerlashuk.dto.comment;

import lombok.Data;

@Data
public class CommentUpdateDTO {

    private Long id;
    private int ratingOfTrainer;
    private String message;
    //private boolean published;

}
