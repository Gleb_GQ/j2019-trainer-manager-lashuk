package by.itstep.j2019trainermanagerlashuk.dto.comment;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import lombok.Data;

@Data
public class CommentFullDTO {

    private Long id;
    private int ratingOfTrainer;
    private String message;
    private UserPreviewDTO user;
    private TrainerPreviewDTO trainer;
    private boolean published;

}
