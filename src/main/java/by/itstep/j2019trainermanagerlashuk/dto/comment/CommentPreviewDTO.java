package by.itstep.j2019trainermanagerlashuk.dto.comment;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserPreviewDTO;
import lombok.Data;

@Data
public class CommentPreviewDTO {

    private Long id;
    private int ratingOfTrainer;
    private String message;
    private UserPreviewDTO user;
    private TrainerPreviewDTO trainer;

}
