package by.itstep.j2019trainermanagerlashuk.dto.trainer;

import lombok.Data;

@Data
public class TrainerPreviewDTO {

    private Long id;
    private String name;
    private String description;
    private String imageUrl;
    private String email;
    private int experience;

}
