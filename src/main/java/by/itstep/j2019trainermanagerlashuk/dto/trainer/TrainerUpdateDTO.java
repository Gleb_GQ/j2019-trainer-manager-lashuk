package by.itstep.j2019trainermanagerlashuk.dto.trainer;

import lombok.Data;

@Data
public class TrainerUpdateDTO {

    private Long id;
    private String name;
    private String description;
    private String password;
    private String imageUrl;
    private String email;
    //private int experience; <- не нужен ведь?

}
