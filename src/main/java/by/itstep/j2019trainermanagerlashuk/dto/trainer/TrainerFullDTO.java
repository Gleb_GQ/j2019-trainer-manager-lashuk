package by.itstep.j2019trainermanagerlashuk.dto.trainer;

import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentPreviewDTO;
import lombok.Data;

import java.util.List;

@Data
public class TrainerFullDTO {

    private Long id;
    private String name;
    private String description;
    private String imageUrl;
    private String email;
    private List<CommentPreviewDTO> comments;
    private List<AppointmentPreviewDTO> appointments;
    private int experience;

}
