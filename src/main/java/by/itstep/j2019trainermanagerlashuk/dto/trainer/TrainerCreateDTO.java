package by.itstep.j2019trainermanagerlashuk.dto.trainer;

import lombok.Data;

@Data
public class TrainerCreateDTO {

    private String name;
    private String password;
    private String description;
    private String imageUrl;
    private String email;
    private int experience;

}

