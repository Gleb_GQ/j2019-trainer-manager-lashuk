package by.itstep.j2019trainermanagerlashuk.dto.user;

import lombok.Data;

@Data
public class UserCreateDTO {

    private String name;
    private String lastName;
    private String email;
    private String password;
    private String imageUrl;

}
