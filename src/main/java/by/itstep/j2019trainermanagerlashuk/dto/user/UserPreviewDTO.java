package by.itstep.j2019trainermanagerlashuk.dto.user;

import lombok.Data;

import java.util.List;

@Data
public class UserPreviewDTO {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private String imageUrl;
}
