package by.itstep.j2019trainermanagerlashuk.dto.user;

import by.itstep.j2019trainermanagerlashuk.dto.appointment.AppointmentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.comment.CommentPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.entity.Role;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class UserFullDTO {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private String imageUrl;
    private List<CommentPreviewDTO> comments;
    private List<AppointmentPreviewDTO> appointments;
    private Set<Role> roles;

}
