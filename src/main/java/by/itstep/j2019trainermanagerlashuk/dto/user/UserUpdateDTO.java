package by.itstep.j2019trainermanagerlashuk.dto.user;

import lombok.Data;

@Data
public class UserUpdateDTO {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private String imageUrl;
    private String password;

}
