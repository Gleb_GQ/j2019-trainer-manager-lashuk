package by.itstep.j2019trainermanagerlashuk.dto.appointment;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import lombok.Data;

import java.sql.Date;
import java.time.LocalDateTime;

@Data
public class AppointmentUpdateDTO {

    private Long id;
    private int trainerId;
    private LocalDateTime appointmentDate;
    private String comment;

}
