package by.itstep.j2019trainermanagerlashuk.dto.appointment;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserPreviewDTO;
import lombok.Data;

import java.sql.Date;
import java.time.LocalDateTime;

@Data
public class AppointmentFullDTO {

    private Long id;
    private UserPreviewDTO user;
    private TrainerPreviewDTO trainer;
    private LocalDateTime appointmentDate;
    private String comment;

}
