package by.itstep.j2019trainermanagerlashuk.dto.appointment;

import by.itstep.j2019trainermanagerlashuk.dto.trainer.TrainerPreviewDTO;
import by.itstep.j2019trainermanagerlashuk.dto.user.UserPreviewDTO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;
import java.time.LocalDateTime;

@Data
public class AppointmentCreateDTO {

    private Long userId;
    private Long trainerId;
    private String comment;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime appointmentDate;

}
