package by.itstep.j2019trainermanagerlashuk;

import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.entity.Role;
import by.itstep.j2019trainermanagerlashuk.repository.UserRepository;
import by.itstep.j2019trainermanagerlashuk.repository.UserRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootTest
public class UserRepositoryImplTests {

    private UserRepository userRepository = new UserRepositoryImpl();
    private List<User> testsUsers;

    public UserRepositoryImplTests(){
        testsUsers = new ArrayList<>();
        testsUsers.add(
                User.builder()
                        .name("test-name-1")
                        .lastName("test-last-name-1")
                        .password("test-password-1")
                        .email("test-email-1")
                        .roles(Collections.singleton(new Role(1L, "USER_ROLE"))).build()
        );
        testsUsers.add(
                User.builder()
                        .name("test-name-2")
                        .lastName("test-last-name-2")
                        .password("test-password-2")
                        .email("test-email-2")
                        .roles(Collections.singleton(new Role(1L, "USER_ROLE"))).build()
        );
    }


    @BeforeEach
    void setUp(){
        userRepository.deleteAll();
    }

    @Test
    void testCreate(){
        //given
        User UserToSave = testsUsers.get(0);

        //when
        User saved = userRepository.create(UserToSave);
        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll(){
        //given
        for(User User : testsUsers){
            userRepository.create(User);
        }

        //when

        List<User> foundUsers = userRepository.findAll();

        //then
        Assertions.assertEquals(2, foundUsers.size());
        for(User foundUser : foundUsers){
            Assertions.assertNotNull(foundUser.getId());
        }
    }

    @Test
    void testFindById(){
        //given
        for(User User : testsUsers){
            userRepository.create(User);
        }

        //when

        User foundUser = userRepository.findById(testsUsers.get(0).getId());

        //then

        Assertions.assertEquals(testsUsers.get(0).getId(), foundUser.getId());
    }

    @Test
    void testDeleteById(){
        //given
        for(User User : testsUsers){
            userRepository.create(User);
        }

        //when

        long idOfUserToDelete = userRepository.findAll().get(0).getId();
        userRepository.deleteById(idOfUserToDelete);

        //then

        Assertions.assertEquals(testsUsers.size() - 1, userRepository.findAll().size());
    }


    @Test
    void testUpdate(){
        //given
        for(User User : testsUsers){
            userRepository.create(User);
        }

        User UserToUpdate = userRepository.findAll().get(0);
        UserToUpdate.setName("changed name");
        //when

        User updatedUser = userRepository.update(UserToUpdate);

        //then

        Assertions.assertEquals("changed name", userRepository.findById(updatedUser.getId()).getName());
    }
}
