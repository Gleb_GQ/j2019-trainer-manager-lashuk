package by.itstep.j2019trainermanagerlashuk;

import by.itstep.j2019trainermanagerlashuk.entity.Comment;
import by.itstep.j2019trainermanagerlashuk.entity.Role;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@SpringBootTest
public class CommentRepositoryImplTests {

    private CommentRepository commentRepository = new CommentRepositoryImpl();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private UserRepository userRepository = new UserRepositoryImpl();
    private List<Comment> testsComments = new ArrayList<>();
    private Trainer trainer;
    private User user;

    @BeforeEach
    void setUp(){

        commentRepository.deleteAll();
        testsComments.clear();
        trainerRepository.deleteAll();
        userRepository.deleteAll();


        user = User.builder()
                .name("test-name").lastName("test-last-name")
                .password("test-password").email("test-email")
                .appointments(null).comments(null)
                .roles(Collections.singleton(new Role(1L, "ROLE_USER"))).build();
        user = userRepository.create(user);

        trainer = Trainer.builder()
                .name("test-name").email("test-email")
                .password("test-password").imageUrl("avatarUrl")
                .comments(null).description("test-description")
                .appointments(null).experience(1).build();
        trainerRepository.create(trainer);

        testsComments.add(
                Comment.builder()
                        .ratingOfTrainer(1)
                        .published(false)
                        .trainer(trainer)
                        .user(user)
                        .build()
        );
        testsComments.add(
                Comment.builder()
                        .ratingOfTrainer(2)
                        .published(false)
                        .trainer(trainer)
                        .user(user)
                        .build()
        );
    }

    @Test
    void testCreate(){
        //given
        Comment CommentToSave = testsComments.get(0);

        //when
        Comment saved = commentRepository.create(CommentToSave);
        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll(){
        //given
        for(Comment Comment : testsComments){
            commentRepository.create(Comment);
        }

        //when

        List<Comment> foundComments = commentRepository.findAll();

        //then
        Assertions.assertEquals(2, foundComments.size());
        for(Comment foundComment : foundComments){
            Assertions.assertNotNull(foundComment.getId());
        }
    }

    @Test
    void testFindById(){
        //given
        for(Comment Comment : testsComments){
            commentRepository.create(Comment);
        }

        //when

        Comment foundComment = commentRepository.findById(testsComments.get(0).getId());

        //then

        Assertions.assertEquals(testsComments.get(0).getId(), foundComment.getId());
    }

    @Test
    void testDeleteById(){
        //given
        for(Comment Comment : testsComments){
            commentRepository.create(Comment);
        }

        //when

        long idOfCommentToDelete = commentRepository.findAll().get(0).getId();
        commentRepository.deleteById(idOfCommentToDelete);

        //then

        Assertions.assertEquals(testsComments.size() - 1, commentRepository.findAll().size());
    }


    @Test
    void testUpdate(){
        //given
        for(Comment comment : testsComments){
            commentRepository.create(comment);
        }

        Comment commentToUpdate = commentRepository.findAll().get(0);
        int oldRating = commentToUpdate.getRatingOfTrainer();

        //when
        commentToUpdate.setRatingOfTrainer(oldRating - 1);
        Comment updatedComment = commentRepository.update(commentToUpdate);

        //then

        Assertions.assertEquals(oldRating - 1, commentRepository.findById(updatedComment.getId()).getRatingOfTrainer());
    }
}
