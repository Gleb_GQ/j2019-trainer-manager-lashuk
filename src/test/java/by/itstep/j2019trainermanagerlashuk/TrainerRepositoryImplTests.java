package by.itstep.j2019trainermanagerlashuk;

import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.entity.Role;
import by.itstep.j2019trainermanagerlashuk.repository.TrainerRepository;
import by.itstep.j2019trainermanagerlashuk.repository.TrainerRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class TrainerRepositoryImplTests {

    private TrainerRepository repository = new TrainerRepositoryImpl();
    private List<Trainer> testsTrainers;

    public TrainerRepositoryImplTests(){
        testsTrainers = new ArrayList<>();
        testsTrainers.add(
                Trainer.builder()
                        .name("test-name-1")
                        .email("test-email-1")
                        .imageUrl("test-url-1")
                        .description("test-description-1")
                        .comments(null)
                        .experience(1)
                        .password("test-pass-1")
                        .appointments(null)
                        .build()
        );
        testsTrainers.add(
                Trainer.builder()
                        .name("test-name-2")
                        .email("test-email-2")
                        .imageUrl("test-url-2")
                        .description("test-description-2")
                        .comments(null)
                        .experience(2)
                        .password("test-pass-2")
                        .appointments(null)
                        .build()
        );
    }


    @BeforeEach
    void setUp(){
        repository.deleteAll();
    }

    @Test
    void testCreate(){
        //given
        Trainer TrainerToSave = testsTrainers.get(0);

        //when
        Trainer saved = repository.create(TrainerToSave);
        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll(){
        //given
        for(Trainer Trainer : testsTrainers){
            repository.create(Trainer);
        }

        //when

        List<Trainer> foundTrainers = repository.findAll();

        //then
        Assertions.assertEquals(2, foundTrainers.size());
        for(Trainer foundTrainer : foundTrainers){
            Assertions.assertNotNull(foundTrainer.getId());
        }
    }

    @Test
    void testFindById(){
        //given
        for(Trainer Trainer : testsTrainers){
            repository.create(Trainer);
        }

        //when

        Trainer foundTrainer = repository.findById(testsTrainers.get(0).getId());

        //then

        Assertions.assertEquals(testsTrainers.get(0).getId(), foundTrainer.getId());
    }

    @Test
    void testDeleteById(){
        //given
        for(Trainer Trainer : testsTrainers){
            repository.create(Trainer);
        }

        //when

        long idOfTrainerToDelete = repository.findAll().get(0).getId();
        repository.deleteById(idOfTrainerToDelete);

        //then

        Assertions.assertEquals(testsTrainers.size() - 1, repository.findAll().size());
    }


    @Test
    void testUpdate(){
        //given
        for(Trainer Trainer : testsTrainers){
            repository.create(Trainer);
        }

        Trainer TrainerToUpdate = repository.findAll().get(0);
        TrainerToUpdate.setName("changed name");
        //when

        Trainer updatedTrainer = repository.update(TrainerToUpdate);

        //then

        Assertions.assertEquals("changed name", repository.findById(updatedTrainer.getId()).getName());
    }
}
