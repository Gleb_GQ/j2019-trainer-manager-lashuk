package by.itstep.j2019trainermanagerlashuk;

import by.itstep.j2019trainermanagerlashuk.entity.Appointment;
import by.itstep.j2019trainermanagerlashuk.entity.Role;
import by.itstep.j2019trainermanagerlashuk.entity.Trainer;
import by.itstep.j2019trainermanagerlashuk.entity.User;
import by.itstep.j2019trainermanagerlashuk.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.security.PrivateKey;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootTest
public class AppointmentRepositoryImplTests {

    private AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private UserRepositoryImpl userRepository = new UserRepositoryImpl();
    private List<Appointment> testsAppointments = new ArrayList<>();
    private User user;
    private Trainer trainer;


    @BeforeEach
    void setUp(){

        appointmentRepository.deleteAll();
        userRepository.deleteAll();
        trainerRepository.deleteAll();
        testsAppointments.clear();

        user = User.builder()
                .name("test-name").lastName("test-last-name")
                .password("test-password").email("test-email")
                .appointments(null).comments(null)
                .roles(Collections.singleton(new Role(1L, "ROLE_USER"))).build();
        user = userRepository.create(user);

        trainer = Trainer.builder()
                .name("test-name").email("test-email")
                .password("test-password").imageUrl("avatarUrl")
                .comments(null).description("test-description")
                .appointments(null).experience(1).build();
        trainer = trainerRepository.create(trainer);

        testsAppointments.add(
                Appointment.builder()
                        .appointmentDate(LocalDateTime.now())
                        .user(user)
                        .trainer(trainer)
                        .build()
        );
        testsAppointments.add(
                Appointment.builder()
                        .appointmentDate(LocalDateTime.now())
                        .trainer(trainer)
                        .user(user)
                        .build()
        );

    }

    @Test
    void testCreate(){
        //given
        Appointment AppointmentToSave = testsAppointments.get(0);

        //when
        Appointment saved = appointmentRepository.create(AppointmentToSave);
        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll(){
        //given
        for(Appointment Appointment : testsAppointments){
            appointmentRepository.create(Appointment);
        }

        //when

        List<Appointment> foundAppointments = appointmentRepository.findAll();

        //then
        Assertions.assertEquals(2, foundAppointments.size());
        for(Appointment foundAppointment : foundAppointments){
            Assertions.assertNotNull(foundAppointment.getId());
        }
    }

    @Test
    void testFindById(){
        //given
        for(Appointment Appointment : testsAppointments){
            appointmentRepository.create(Appointment);
        }

        //when

        Appointment foundAppointment = appointmentRepository.findById(testsAppointments.get(0).getId());

        //then

        Assertions.assertEquals(testsAppointments.get(0).getId(), foundAppointment.getId());
    }

    @Test
    void testDeleteById(){
        //given
        for(Appointment Appointment : testsAppointments){
            appointmentRepository.create(Appointment);
        }

        //when

        appointmentRepository.deleteById((appointmentRepository.findAll().get(0).getId()));

        //then

        Assertions.assertEquals(testsAppointments.size() - 1, appointmentRepository.findAll().size());
    }


    @Test
    void testUpdate(){
        //given
        for(Appointment Appointment : testsAppointments){
            appointmentRepository.create(Appointment);
        }

        Appointment AppointmentToUpdate = appointmentRepository.findAll().get(0);
        AppointmentToUpdate.setComment("updated-comment");

        Appointment updatedAppointment = appointmentRepository.update(AppointmentToUpdate);

        //then
        Appointment actualAppointment = appointmentRepository.findById(updatedAppointment.getId());
        Assertions.assertEquals("updated-comment", actualAppointment.getComment());
    }
}
